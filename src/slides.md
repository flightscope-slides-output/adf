---
title: Automatic Direction Finder (ADF)
---

## Handout { data-transition="zoom-in slide-out" }

<p style="font-size: x-large; font-weight: bold; text-align: center">
  <a href="https://flightscope.gitlab.io/training/handouts/adf.pdf">
    Link to Handout
    <br>
    <img src="https://flightscope.gitlab.io/training/handouts/adf.png" width="400" alt="ADF handout"/>
  </a>
</p>

---

## Aims and Objectives { data-transition="zoom-in slide-out" }

###### Aims { data-transition="fade-in slide-out" }

* understand the principles of how an ADF works
* understand the principles of ADF operation
* understand the principles of ADF orientation
* understand the principles of track intercepts using the ADF

---

## Aims and Objectives { data-transition="fade-in slide-out" }

###### Objectives { data-transition="fade-in slide-out" }

* explain the workflow associated with setting up the ADF
* recall three errors experienced by the ADF
* explain the five ADF navigation aid interception questions

---

## ADF Introduction { data-transition="zoom-in slide-out" }

---

## ADF Introduction { data-transition="slide-in slide-out" }

###### Radio Waves

* Radio waves are transmitted from an antenna in all directions at the speed of light
* Frequency is a measure of the number of crests per second
* 1 radio wave crest per second is 1 Hertz
  * One thousand Hertz = 1 Kilohertz = 1 kHz
  * One million Hertz = 1 Megahertz = 1 MHz

<p style="text-align: center">
  <img src="images/wave.png" alt="Radio Wave" width="600px"/>
</p>

---

## ADF Introduction { data-transition="slide-in slide-out" }

###### Radio Wave Propagation

* Ground waves are in the frequency range 50-250 kHz
* Ground waves travel through the Earth's surface
* Sky waves are in the frequency range 3-30 MHz
* Sky waves reflect or refract from the ionosphere and return to the ground
* Space waves are in the frequency range 30-300 MHz
* Space waves propagate through the atmosphere to outer space

<p style="text-align: center">
  <img src="images/wave-propagation.png" alt="Radio Wave Propagation" width="500px"/>
</p>

---

## ADF Introduction { data-transition="slide-in slide-out" }

###### Equipment

* **NDB:** Non-Directional Beacon, located on the ground
* Operates in the frequency range 200-500 kHz
* May be _pilot-monitored_, requiring a pilot report of faults
  * excessive hum
  * low power
  * loss of ident

<p style="text-align: center">
  <img src="images/ndb.png" alt="NDB" width="150px"/>
</p>

---

## ADF Introduction { data-transition="slide-in slide-out" }

###### Equipment

* **ADF:** Automatic Direction Finder, onboard the aircraft
  * **Fixed Card ADF:** A display of 360&deg; compass rose, unable to adjust
  * **Rotating Card ADF:** A display of 360&deg; compass rose, able to be adjusted by the pilot _(most common)_

<p style="text-align: center">
  <img src="images/adf-rotating-card.png" alt="ADF Rotating Card" width="350px"/>
</p>

---

## ADF Introduction { data-transition="slide-in slide-out" }

###### Equipment

* Made up of two antennae
  1. Loop: determines the axis of the radio signal
  2. Sense: determines the direction of the radio signal along that axis
* Located under the fuselage of the aircraft

<p style="text-align: center">
  <img src="images/adf-antenna.png" alt="ADF Antenna" width="350px"/>
</p>

---

## ADF Introduction { data-transition="slide-in slide-out" }

###### Equipment in the cockpit

* Fixed or Rotating Card ADF
* Tunable ADF Radio Receiver

<p style="text-align: center">
  <img src="images/adf-and-radio.png" alt="ADF and Radio" width="350px"/>
</p>

---

## ADF Introduction { data-transition="slide-in slide-out" }

###### Equipment

* This system is very similar to the AM radio in a car
  * A station transmits AM radio waves (NDB)
  * A AM receiver in the car interprets those radio waves (ADF)
  * Difference:
    * the ADF points to the direction of the source of the radio waves
    * the car radio converts the radio signals to sound waves

---

## ADF Errors { data-transition="zoom-in slide-out" }

---

## ADF Errors { data-transition="slide-in slide-out" }

###### Summary

* Night effect
* Terrain effect
* Mountain effect
* Thunderstorm activity
* Co-channel Interference
* Coastal refraction
* Quadrantal error

---

## ADF Errors { data-transition="slide-in slide-out" }

###### Night Effect

* The structure of the ionosphere changes at night
* Reflected sky waves arrive later to the receiver than the ground wave
* The result is a reduction in useful range of the NDB at night

<p style="text-align: center">
  <img src="images/ionosphere.png" alt="Ionosphere" width="550px"/>
</p>

---

## ADF Errors { data-transition="slide-in slide-out" }

###### Terrain Effect

* The ground wave travels more efficiently through water
* Over land, the ground wave attenuates
* The result is a greater range of the NDB over water than land

---

## ADF Errors { data-transition="slide-in slide-out" }

###### Mountain Effect

* The sky wave may be reflected by a nearby mountain
* The result is similar to night effect
* Mountain effect can be managed by climbing to a higher altitude

<p style="text-align: center">
  <img src="images/mountain-effect.png" style="border: 2px solid #dcdcdc; padding: 10px" alt="Mountain Effect" width="550px"/>
</p>

---

## ADF Errors { data-transition="slide-in slide-out" }

###### Thunderstorm Activity

* Lightning transmits radio waves at a similar frequency to the NDB
* The ADF indication swings between the last lightning location and the NDB
* The ADF becomes unusable

<p style="text-align: center">
  <img src="images/lightning.png" alt="Lightning" width="550px"/>
</p>

---

## ADF Errors { data-transition="slide-in slide-out" }

###### Co-channel Interference

* Interference caused by a station transmitting on the same or similar frequency to the NDB
* The ADF indication will be random

---

## ADF Errors { data-transition="slide-in slide-out" }

###### Coastal Refraction

* When a ground wave crosses the coast at 90&deg;, it continues in the same direction
* When a ground wave crosses the coast otherwise, it bends _(refracts)_ towards the coast line

<p style="text-align: center">
  <img src="images/coastal-refraction.png" alt="Coastal Refraction" width="550px"/>
</p>

---

## ADF Errors { data-transition="slide-in slide-out" }

###### Quadrantal Error

* The radio signal is distorted when it encounters the aircraft structure
* The ADF indicates the direction of the signal as it arrives at the antenna
* This effect is most common on bearings which are multiples of 45&deg;

<p style="text-align: center">
  <img src="images/quadrantal-error.png" style="border: 2px solid #dcdcdc; padding: 10px" alt="Quadrantal Error" width="400px"/>
</p>

---

## ADF Coverage { data-transition="zoom-in slide-out" }

---

## ADF Coverage { data-transition="slide-in slide-out" }

###### ADF Rated Coverage

* The rated coverage of each NDB depends on:
  * terrain between aircraft and NDB station
  * day or night
  * transmitter power

<p style="text-align: center">
  <img src="images/aip-bromelton-ndb.png" style="border: 2px solid #dcdcdc; padding: 10px" alt="AIP Bromelton NDB" width="750px"/>
</p>

---

## ADF Coverage { data-transition="slide-in slide-out" }

###### ADF Broadcast Stations

* Until 2020, broadcasting radio stations and their frequency were published in the ERSA
* The location, identifier, power output and frequency of the station were provided
* A pilot was able to listen to the cricket on ABC by tuning the ADF to the correct frequency
* AirServices no longer publishes information on these stations
* Today they can be found on the ACMA website

---

## Orientation and Tracking { data-transition="zoom-in slide-out" }

---

## Orientation and Tracking { data-transition="slide-in slide-out" }

###### ADF Setup

###### T.I.T.

* <span class="fragment highlight-red"><strong>T</strong></span>une: _set the correct NDB frequency on the ADF_
* <span class="fragment highlight-red"><strong>I</strong></span>dentify: _listen for the NDB morse identifier_
* <span class="fragment highlight-red"><strong>T</strong></span>est: _test the ADF is functioning correctly_

<p style="text-align: center">
  <img src="images/adf-and-radio.png" alt="ADF and Radio" width="350px"/>
</p>

---

## Orientation and Tracking { data-transition="slide-in slide-out" }

###### Fixed Card ADF

* **Relative Bearing**: the direction towards the NDB, relative to your heading
* <code><strong>0</strong></code>: the nose of the aircraft
* <code><strong>9</strong></code>: the right wing of the aircraft
* <code><strong>18</strong></code>: the tail of the aircraft
* <code><strong>27</strong></code>: the left wing of the aircraft

<p style="text-align: center">
  <img src="images/adf-fixed-card.png" alt="ADF Fixed Card" width="570px"/>
</p>

---

## Orientation and Tracking { data-transition="slide-in slide-out" }

###### Orientation

* Transpose the orientation of the ADF needle on to the Heading Indicator (HI)
* Imagine:
  * centre of HI is the NDB station
  * aircraft is at the base of HI
  * track to NDB is at the head of HI

---

## Orientation and Tracking { data-transition="slide-in slide-out" }

###### Orientation

* To begin orientating ourselves, we can ask five questions
  1. Where am I?
  2. Where do I want to be?
  3. Which way do I turn?
  4. On to which heading?
  5. How do I know I am there?

---

## Orientation and Tracking { data-transition="slide-in slide-out" }

###### Orientation &mdash; Where am I?

* on heading 300&deg;
* on 360&deg; relative bearing to station
* track 360&deg; to station

<p style="text-align: center">
  <img src="images/heading-300-adf-360.png" alt="Heading 300, ADF 360" width="570px"/>
</p>

---

## Orientation and Tracking { data-transition="slide-in slide-out" }

###### Orientation &mdash; Where am I?

* on heading 135&deg;
* on 225&deg; relative bearing to NDB station
* track 360&deg; to station

<p style="text-align: center">
  <img src="images/heading-135-adf-225-transposed.png" alt="Heading 135, ADF 225" width="440px"/>
</p>

---

## Orientation and Tracking { data-transition="slide-in slide-out" }

###### Orientation &mdash; Where am I?

* on heading 270&deg;
* on 045&deg; relative bearing to NDB station
* track 315&deg; to station

<p style="text-align: center">
  <img src="images/heading-270-adf-045-transposed.png" alt="Heading 270, ADF 045" width="440px"/>
</p>

---

## Orientation and Tracking { data-transition="slide-in slide-out" }

###### Intercept &mdash; Where do I want to be?

* **Intercept**: the heading used to cross the required track to be intercepted
* **Intercept angle**: the angle between the intercept heading and required track
* For intercept angles, use 30&deg;, 45&deg; or 60&deg; for easy calculation

<p style="text-align: center">
  <img src="images/aircraft-tracking-ndb-intercept-track.png" style="border: 2px solid #dcdcdc; padding: 10px" alt="Aircraft Intercept and Intercept Track" width="380px"/>
</p>

---

## Orientation and Tracking { data-transition="slide-in slide-out" }

###### Intercept heading &mdash; Where do I want to be?

<p style="font-size: x-large; text-align: center">
  Once on intercept track, adjust for crosswind using the angle/percent rule of thumb
</p>

<p style="text-align: center">
  <img src="images/crosswind-correction-rule-of-thumb.png" style="border: 2px solid #dcdcdc; padding: 10px" alt="30/45/60 rule of thumb" width="570px"/>
</p>

---

## Orientation and Tracking { data-transition="slide-in slide-out" }

###### Intercept heading &mdash; crosswind correction

* For example
  * Track <code>360&deg;</code>
  * Wind: <code>04015KT</code>
  * Heading: <code><strong>005&deg;</strong></code>

<p style="text-align: center">
  <img src="images/heading-360-adf-360.png" alt="Heading 360, ADF 360" width="570px"/>
</p>

---

## Orientation and Tracking { data-transition="slide-in slide-out" }

###### ADF Intercept &mdash; Inbound to Inbound

<p style="text-align: center">
  <img src="images/aircraft-tracking-ndb-inbound-inbound-060-030.png" style="border: 2px solid #dcdcdc; padding: 10px" alt="Aircraft tracking NDB Inbound to Inbound" width="750px"/>
</p>

---

## Orientation and Tracking { data-transition="slide-in slide-out" }

###### ADF Intercept &mdash; Outbound to Outbound

<p style="text-align: center">
  <img src="images/aircraft-tracking-ndb-outbound-outbound-180-210.png" style="border: 2px solid #dcdcdc; padding: 10px" alt="Aircraft tracking NDB Outbound to Outbound" width="650px"/>
</p>

---

## Orientation and Tracking { data-transition="slide-in slide-out" }

###### ADF Intercept &mdash; Inbound to Outbound

<p style="text-align: center">
  <img src="images/aircraft-tracking-ndb-inbound-outbound-045-180.png" style="border: 2px solid #dcdcdc; padding: 10px" alt="Aircraft tracking NDB Inbound to Outbound" width="650px"/>
</p>

---

## Orientation and Tracking { data-transition="slide-in slide-out" }

###### ADF Intercept &mdash; Outbound to Inbound

<p style="text-align: center">
  <img src="images/aircraft-tracking-ndb-outbound-inbound-240-030.png" style="border: 2px solid #dcdcdc; padding: 10px" alt="Aircraft tracking NDB Outbound to Inbound" width="750px"/>
</p>

---

## Airmanship/TEM/HF { data-transition="convex-in slide-out" }

<section>
  <table>
      <thead>
        <tr style="font-size: xx-large">
            <th>Threat</th>
            <th>Error</th>
            <th>Undesired State</th>
            <th>Mitigation</th>
        </tr>
      </thead>
      <tbody>
        <tr style="font-size: x-large">
          <td>Fatigue</td>
          <td>Failure to scan instruments</td>
          <td>Unusual Attitude</td>
          <td>I.M.S.A.F.E.</td>
        </tr>
        <tr style="font-size: x-large">
          <td>Misidentified Unusual Attitude</td>
          <td>Inappropriate pilot inputs</td>
          <td>Unusual Attitude</td>
          <td>Recognise symptoms of unusual attitude</td>
        </tr>
        <tr style="font-size: x-large">
          <td>Traffic conflict</td>
          <td>Loss of situational awareness</td>
          <td>Airprox, collision</td>
          <td>Lookout scan, Listening</td>
        </tr>
        <tr style="font-size: x-large">
          <td>IMC</td>
          <td>Unanticipated weather</td>
          <td>C.F.I.T.</td>
          <td>Obtain current and complete weather forecast</td>
        </tr>
      </tbody>
    </table>
</section>

---

## Pilot Fitness Checklist { data-transition="convex-in zoom-out" }

<p style="text-align: center">
  <img src="images/imsafe.png" alt="IMSAFE" height="550px"/>
</p>

---

## Quiz on Objectives { data-transition="convex-in slide-out" }

* What are the three steps to setting up an ADF before using?
* What are three of the possible errors experienced by an ADF?
* What are the five questions to ask for navigation aid interception?
